package com.marcs100gmail.mnotes;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.content.ContentValues;
import java.util.List;
import java.util.ArrayList;



public class notebookActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ListView notebooksList;
    private boolean activityStarted=false;


    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        this.activityStarted=true;

        Log.d("notebookActivity", "in onCreate(Bundle savedInstanceState)");

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        this.displayNotebooks();

    }

    private void displayNotebooks(){
        DatabaseHelper db = new DatabaseHelper(this);
        Log.d("notebookActivity", "in displayNotebooks");
        //db.insertDummyValues();

        //The first time round notebooksText = null
        //not sure why - but that is what we must first check if it is null else we will
        //get a null pinter exception if we try to use notebooksText

        notebooksList = (ListView) findViewById(R.id.notebooksListView);

        //TextView txtOut = (TextView) findViewById(R.id.txtOutput); //** THIS IS NULL!!!!!!!!!!!
        if (notebooksList != null){

            this.notebooksList.setVisibility(View.VISIBLE);


             String[] results = db.getNotebooks();
             if (results != null) {
                notebooksList.setAdapter(new ArrayAdapter<String>(notebooksList.getContext(), android.R.layout.simple_list_item_1, results));
                notebooksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        //Toast.makeText(notebookActivity.this, "Notebook list clicked", Toast.LENGTH_LONG).show();
                        //displayNotesInNotebook(v, position, id);

                        //START NOTES ACTIVITY HERE!!!!!
                        Log.d("notebookActivity", "starting notesActivity)");
                        Intent intent = new Intent(getApplicationContext(), notesActivity.class);
                        //Sending data to another Activity
                        intent.putExtra("notebookName", (String) notebooksList.getItemAtPosition(position));

                        startActivityForResult(intent, 1);
                        //finish();
                    }
                });
             }



        }
    }



    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        //FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.beginTransaction()
        //        .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
        //        .commit();

        Log.d("notebookActivity", "in onNavigationDrawerItemSelected");
        //if (this.activityStarted==true) {
            //    finish();
        //    Intent intent = new Intent(getApplicationContext(), mainActivity.class);
        //    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //    intent.putExtra("position",position);
        //    Log.d("notebookActivity","starting mainActivity");
        //    startActivityForResult(intent,1);
        //    finish();
        //}

        if (this.activityStarted==true) {
            Intent i = new Intent();
            i.putExtra("position", position);
            setResult(1, i);
            finish();
        }
    }

    public void onActivityResult(int requestCode, int ResultCode, Intent data){
        Log.d("notebookActivity","checking result");
        Intent i = new Intent();
        i.putExtra("position",data.getIntExtra("position",-1));
        setResult(1, i);
        finish();

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("position", -1);
        setResult(1, i);
        finish();
        super.onBackPressed();
    }

    public void onSectionAttached(int number) {


        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.notes, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
