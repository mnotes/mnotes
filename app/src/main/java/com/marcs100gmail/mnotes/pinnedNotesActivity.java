package com.marcs100gmail.mnotes;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class pinnedNotesActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private boolean activityStarted=false;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ListView pinnedNotesList;
    private final int NOTE_DISPLAY_LEN=1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinned_notes);

        this.activityStarted=true;

        Log.d("pinnedNotesActivity", "in onCreate(Bundle savedInstanceState)");

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        //mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }


    protected void onStart(){
        super.onStart();
        displayPinnedNotes();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("position", -1); //we must have come from main activity so no drawer to select
        setResult(1, i);
        finish();
        super.onBackPressed();
    }


    private void displayPinnedNotes(){
        Log.d("notesActivity", "in displayNotesInNotebook");
        pinnedNotesList = (ListView) findViewById(R.id.pinnedNotesListView);
        DatabaseHelper db = new DatabaseHelper(this);
        List<ContentValues> dataRows = db.getPinnedNotes();

        String[] listItems = new String[dataRows.size()];
        ArrayList<NoteDetails> noteItems = new ArrayList<NoteDetails>();
        for (ContentValues rowData: dataRows) {

            String noteContent = (String) rowData.get("content");
            if (noteContent.length() > NOTE_DISPLAY_LEN) {
                noteContent = noteContent.substring(0, NOTE_DISPLAY_LEN);
            }

            NoteDetails noteDetail = new NoteDetails((String)rowData.get("noteID"),(String) rowData.get("notebook"),
                    (String) rowData.get("tag"),noteContent,"","",(String) rowData.get("BGColour"),"");

            noteItems.add(noteDetail);
        }
        pinnedNotesList.setAdapter(new PinnedNotesCustomAdapter(pinnedNotesList.getContext(), noteItems));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pinned_notes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (this.activityStarted==true) {
            Intent i = new Intent();
            i.putExtra("position", position);
            setResult(1, i);
            finish();
        }

    }
}
