package com.marcs100gmail.mnotes;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.graphics.Color;

import java.util.ArrayList;

/**
 * Created by marc on 07/10/14.
 */
public class NotesCustomAdapter extends ArrayAdapter<NoteDetails> {

    public NotesCustomAdapter(Context context, ArrayList<NoteDetails> noteDetails) {
        super(context, 0, noteDetails);
    }

    private String noteID="";

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        NoteDetails note= getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.notes_layout_item2, parent, false);
        }

        //save sqlID so it can be retrieved later
        //this.noteID=note.sqlID;

        //populate the list view
        TextView tag = (TextView) convertView.findViewById((R.id.tag));
        tag.setText("Tag: "+note.tag);
        TextView content = (TextView) convertView.findViewById((R.id.content));
        content.setText(note.contents);

        if(note.colour != null)
        {
            if(note.colour.length()>0) {
                //Log.d("will attempt to set background colour ", note.colour.toUpperCase());
                try {
                    tag.setBackgroundColor(Color.parseColor(note.colour.toUpperCase()));
                    content.setBackgroundColor(Color.parseColor(note.colour.toUpperCase()));
                } catch (Exception e) {
                    Log.e("error", "could not set background colour " + note.colour.toUpperCase());
                }
            }
        }

        return convertView;
    }

    public String getNoteID(int pos){

        return getItem(pos).sqlID;
    }
}
