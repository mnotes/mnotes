package com.marcs100gmail.mnotes;

/**
 * Created by marc on 07/10/14.
 */

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import android.graphics.Color;


/**
 * Created by marc on 07/10/14.
 */
public class PinnedNotesCustomAdapter extends ArrayAdapter<NoteDetails> {

    public PinnedNotesCustomAdapter(Context context, ArrayList<NoteDetails> noteDetails) {
        super(context, 0, noteDetails);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        NoteDetails note= getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.pinned_notes_layout, parent, false);
        }

        //populate the list view
        //TextView notebook = (TextView) convertView.findViewById((R.id.notebookName));
        //notebook.setText("Notebook: "+note.notebook);
        TextView nameAndTag = (TextView) convertView.findViewById((R.id.notebookNameAndTag));
        nameAndTag.setText("Notebook: "+note.notebook+"  Tag: "+note.tag);
        TextView content = (TextView) convertView.findViewById((R.id.content));
        content.setText(note.contents);
        if(note.colour != null)
        {
            if(note.colour.length()>0) {
                //Log.d("will attempt to set background colour ", note.colour.toUpperCase());
                try {
                    nameAndTag.setBackgroundColor(Color.parseColor(note.colour.toUpperCase()));
                    content.setBackgroundColor(Color.parseColor(note.colour.toUpperCase()));
                } catch (Exception e) {
                    Log.e("error", "could not set background colour " + note.colour.toUpperCase());
                }
            }
        }

        return convertView;
    }
}

