package com.marcs100gmail.mnotes;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.provider.ContactsContract;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View.OnTouchListener;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Date;
import java.util.List;

public class notePageActivity extends Activity {
    //private NavigationDrawerFragment mNavigationDrawerFragment;
    //private ListView notesList;
    //private final int NOTE_DISPLAY_LEN=120;
    //private String notebookName;
    private CharSequence mTitle;
    private boolean activityStarted=false;

    private EditText noteContentText;
    private TextView noteTitleText;
    protected String noteID;
    protected int currentPageNumber;
    protected List<String> pages;
    protected int numberOfPages;

    private float x1,x2;
    static final int MIN_DISTANCE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_page);

        this.activityStarted=true;

        Log.d("notePageActivity", "in onCreate(Bundle savedInstanceState)");

        //mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        //mTitle = getTitle();

        // Set up the drawer.
        //mNavigationDrawerFragment.setUp(
                //R.id.navigation_drawer,
                //(DrawerLayout) findViewById(R.id.drawer_layout));
    }

    protected void onStart(){
        super.onStart();
        Intent i = getIntent();
        // Receiving the Data
        // get sql index of note (from this we can also get the current notebook).
        noteID = i.getStringExtra("noteID");
        String notebookName=i.getStringExtra("notebookName");
        this.setTitle(notebookName);

        //get a list of all the note id's for this notebook
        DatabaseHelper db = new DatabaseHelper(this);
        pages = db.getNoteIds(notebookName);
        db.close();

        currentPageNumber = pages.indexOf(noteID);  //set the current page
        numberOfPages=pages.size()-1; //set number of pages in notebook

        //Toast.makeText(this, "noteID is "+noteID, Toast.LENGTH_LONG).show();
        //displayNotesInNotebook(notebookName);
        if (noteID==null){
            Log.e("notePageActivity","No note ID received - nothing to display");
            Toast.makeText(this, "No note ID received - nothing to display", Toast.LENGTH_LONG).show();
        }
        else{
            //noteContentText = (EditText) findViewById((R.id.notepage_contents));
            //implement our listener to detect left and right swipe.
            /*noteContentText.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            x1 = event.getX();
                            break;
                        case MotionEvent.ACTION_UP:
                            x2 = event.getX();
                            float deltaX = x2 - x1;

                            if (Math.abs(deltaX) > MIN_DISTANCE)
                            {
                                // Left to Right swipe action
                                if (x2 > x1)
                                {
                                    //Toast.makeText(getBaseContext(), "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                                    //Toast.makeText(getBaseContext(),noteID, Toast.LENGTH_SHORT).show ();

                                    if ((currentPageNumber+1) <=numberOfPages){
                                        currentPageNumber+=1;
                                        noteID=pages.get(currentPageNumber);
                                        displayNote();

                                    }
                                    else{
                                        Toast.makeText(getBaseContext(), "reached beginning of notebook", Toast.LENGTH_SHORT).show ();
                                    }
                                }

                                // Right to left swipe action
                                else
                                {
                                    //Toast.makeText(getBaseContext(), "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                                    if(currentPageNumber>0) {
                                        currentPageNumber -=1;
                                        noteID=pages.get(currentPageNumber);
                                        displayNote();
                                    }
                                    else{
                                        Toast.makeText(getBaseContext(), "reached end of notebook", Toast.LENGTH_SHORT).show ();
                                    }
                                }

                            }
                            else {
                                // consider as something else - a screen tap for example
                                //return super.onTouchView(v, event);
                                //Toast.makeText(getBaseContext(), "return false", Toast.LENGTH_SHORT).show ();
                                return false;
                            }
                            break;
                        case MotionEvent.ACTION_SCROLL: // this is not detected!!!!!!!
                            Toast.makeText(getBaseContext(), "scroll!!!", Toast.LENGTH_SHORT).show ();
                            break;

                    }
                    return true;
                }
            });*/
            this.displayNote();
        }

    }


    private void displayNote(){
        //To do.. get list of sqlID's for current notebook
        //To do .. Get the note contents for the given noteID.
        DatabaseHelper db = new DatabaseHelper(this);
        ContentValues data = db.getNote(noteID);
        db.close();
        if(data!=null){
            noteTitleText = (TextView) findViewById((R.id.notepage_title));
            noteContentText = (EditText) findViewById((R.id.notepage_contents));
            //noteContentText.setVerticalScrollBarEnabled(true);
            //noteContentText.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            noteContentText.setSingleLine(false);
            String bgColour = (String) data.get("BGColour");
            noteTitleText.setText("N: "+(String)data.get("notebook")+"  T: "+(String)data.get("tag"));
            noteContentText.setText((String)data.get("content"));
            try {
                noteContentText.setBackgroundColor(Color.parseColor(bgColour.toUpperCase()));
                noteTitleText.setBackgroundColor(Color.parseColor(bgColour.toUpperCase()));
            }
            catch(Exception e){
                Toast.makeText(this, "Problem setting background colour", Toast.LENGTH_LONG).show();
            }

            String hash = this.hashNote((String)data.get("content"));
        }
    }

    private String  hashNote(String noteContents){
        String hash="";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(noteContents.getBytes("iso-8859-1"), 0, noteContents.length());
            hash = bytesToHex(md.digest());
            Toast.makeText(this, "HASH of note is " + hash, Toast.LENGTH_LONG).show();
        }
        catch(NoSuchAlgorithmException e){
            Toast.makeText(this, "Unsupported algorithm Exception", Toast.LENGTH_LONG).show();
            return "";
        }
        catch(UnsupportedEncodingException ue){
            Toast.makeText(this, "" +
                    "Unsupported Encoding Exception", Toast.LENGTH_LONG).show();
            return "";
        }
        return hash;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex( byte[] bytes )
    {
        char[] hexChars = new char[ bytes.length * 2 ];
        for( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[ j ] & 0xFF;
            hexChars[ j * 2 ] = hexArray[ v >>> 4 ];
            hexChars[ j * 2 + 1 ] = hexArray[ v & 0x0F ];
        }
        return new String( hexChars );
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("position", 0); //we must have come from notebook activity so select that drawer
        setResult(1, i);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.note_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (this.activityStarted) {
            Intent i = new Intent();
            i.putExtra("position", position);
            setResult(1, i);
            finish();
        }

    }*/
}
