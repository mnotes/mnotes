package com.marcs100gmail.mnotes; /**
 * Created by marc on 18/09/14.
 */
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.content.ContentValues;
import android.widget.Toast;
import android.widget.AdapterView;
import android.content.Context;
import android.widget.AdapterView.OnItemClickListener;


import com.marcs100gmail.mnotes.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;





public class notesActivity extends Activity  implements NavigationDrawerFragment.NavigationDrawerCallbacks{

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ListView notesList;
    private final int NOTE_DISPLAY_LEN=120;
    //private String notebookName;
    private CharSequence mTitle;
    private boolean activityStarted=false;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        this.activityStarted=true;

        Log.d("notesActivity","in onCreate(Bundle savedInstanceState)");

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }


    protected void onStart(){
        super.onStart();
        Intent i = getIntent();
        // Receiving the Data
        String notebookName = i.getStringExtra("notebookName");
        this.setTitle(notebookName);
        displayNotesInNotebook(notebookName);
    }


    private void displayNotesInNotebook(final String notebookName) {
        Log.d("notesActivity", "in displayNotesInNotebook");
        notesList = (ListView) findViewById(R.id.notesListView);
        notesList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                view.setSelected(true);

                //debug
                //TextView contentView = (TextView)view.findViewById(R.id.content);
                //Toast.makeText(notesActivity.this, contentView.getText(), Toast.LENGTH_LONG).show();

                Log.d("notesActivity", "starting notePageActivity)");
                Intent intent = new Intent(getApplicationContext(), notePageActivity.class);

                //Sending data to another Activity
                intent.putExtra("notebookName", notebookName);

                //I need to get the sqlid!!!!!!!!!!
                NotesCustomAdapter adapter= (NotesCustomAdapter) notesList.getAdapter();
                //Toast.makeText(notesActivity.this,adapter.getNoteID(),Toast.LENGTH_LONG).show();
                intent.putExtra("noteID",adapter.getNoteID(position));

                startActivityForResult(intent, 1);
            }
        });

        DatabaseHelper db = new DatabaseHelper(this);
        List<ContentValues> dataRows = db.getNotesInNotebook(notebookName);

        /*
        String[] listItems = new String[dataRows.size()];
        int itemNo=0;
        for (ContentValues rowData: dataRows) {
            //don't show all of the note!
            String noteContent = (String) rowData.get("content");
            if (noteContent.length() > NOTE_DISPLAY_LEN) {
                noteContent = noteContent.substring(0, NOTE_DISPLAY_LEN);
            }

            listItems[itemNo] = "Tag: " + (String) rowData.get("tag") + "\n" + noteContent;
            itemNo++;
        }*/

        ArrayList<NoteDetails> noteItems = new ArrayList<NoteDetails>();
        for (ContentValues rowData: dataRows) {

            String noteContent = (String) rowData.get("content");
            if (noteContent.length() > NOTE_DISPLAY_LEN) {
                noteContent = noteContent.substring(0, NOTE_DISPLAY_LEN);
            }

            NoteDetails noteDetail = new NoteDetails((String)rowData.get("noteID"),(String) rowData.get("notebook"),
                    (String) rowData.get("tag"),noteContent,"","",(String) rowData.get("BGColour"),"");

            noteItems.add(noteDetail);
        }
        //notesList.setAdapter(new ArrayAdapter<String>(notesList.getContext(), R.layout.notes_layout_item, listItems));
        notesList.setAdapter(new NotesCustomAdapter(notesList.getContext(), noteItems));
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        //FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.beginTransaction()
        //        .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
        //        .commit();

        //Toast.makeText(notesActivity.this, "in notes Activity", Toast.LENGTH_LONG).show();
        /*
        Log.d("notesActivity","in onNavigationDrawerItemSelected");
        if (this.activityStarted==true) {
                Intent intent = new Intent(getApplicationContext(), mainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Log.d("notesActivity","position is "+Integer.toString(position));
                intent.putExtra("position",position);
                Log.d("notesActivity","starting mainActivity");
                startActivity(intent);
                finish();
        }*/
        if (this.activityStarted==true) {
            Intent i = new Intent();
            i.putExtra("position", position);
            setResult(1, i);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("position", 0); //we must have come from notebook activity so select that drawer
        setResult(1, i);
        finish();
        super.onBackPressed();
    }

    /*public void onSectionAttached(int number) {


        switch (number) {
            case 1:
                //mTitle = getString(R.string.title_section1);
                break;
            case 2:
                //mTitle = getString(R.string.title_section2);
                break;
            case 3:
                //mTitle = getString(R.string.title_section3);
                break;
        }
    }*/

    /*public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        //actionBar.setTitle(mTitle);
    }*/


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.notes, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/


}
