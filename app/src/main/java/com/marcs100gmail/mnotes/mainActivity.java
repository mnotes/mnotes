package com.marcs100gmail.mnotes;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class mainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ListView notebooksList;


    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        Log.d("mainActivity", "in onCreate(Bundle savedInstanceState)");

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }


    //@Override
    //protected void onStart(){
    //    super.onStart();
    //    Log.d("mainActivity", "in onStart");
        // Receiving the Data



    //}


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        //FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.beginTransaction()
        //        .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
        //        .commit();

        Log.d("mainActivity", "in onNavigationDrawerItemSelected");
        Log.d("mainActivity", "position is "+Integer.toString(position));

        //what is the drawer selected
        switch (position) {
            case 0:
                Intent notebookActivityView = new Intent(getApplicationContext(), notebookActivity.class);
                Log.d("mainActivity", "starting notebookActivity");
                startActivityForResult(notebookActivityView,1);
                break;
            case 1:
                Intent pinnedActivityView = new Intent(getApplicationContext(), pinnedNotesActivity.class);
                Log.d("mainActivity", "starting notebookActivity");
                startActivityForResult(pinnedActivityView ,1);
                break;
            case 2:
                Toast.makeText(this, "Tags selected", Toast.LENGTH_LONG).show();
                break;

        }

    }

    public void onActivityResult(int requestCode, int ResultCode, Intent data){
        int drawerPosition=data.getIntExtra("position",-1);
        if (drawerPosition!=-1){
            this.onNavigationDrawerItemSelected(drawerPosition);
        }
    }



    public void onSectionAttached(int number) {


        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.notes, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
