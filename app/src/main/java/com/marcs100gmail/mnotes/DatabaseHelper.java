package com.marcs100gmail.mnotes; /**
 * Created by marc on 07/09/14.
 */
import android.animation.FloatEvaluator;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.util.List;
import java.util.ArrayList;


public class DatabaseHelper extends SQLiteOpenHelper{

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "marcnotes_db";
    private static final String  DATABASE_PATH = Environment.getExternalStorageDirectory().toString()+ File.separator+"sync";
    private Context myContext;

    public DatabaseHelper(Context context) {
        //super(context, DATABASE_NAME, null, DATABASE_VERSION);
        super(context, DATABASE_PATH+ File.separator+DATABASE_NAME, null, DATABASE_VERSION);
        //Toast.makeText(context, DATABASE_PATH+ File.separator+DATABASE_NAME, Toast.LENGTH_LONG).show();
        myContext=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table

        Toast.makeText(myContext, "creating database", Toast.LENGTH_LONG).show();
        /*String CREATE_TABLE = "CREATE TABLE marcnotes ("+
                "id INTEGER PRIMARY KEY,"+
                "notebook TEXT, "+
                "tag TEXT,"+
                "content TEXT, "+
                "created TEXT, "+
                "modified TEXT, "+
                "pinned INTEGER DEFAULT (0), "+
                "BGColour TEXT)";

        // create books table
        db.execSQL(CREATE_TABLE);*/

        //this.insertDummyValues();
    }

    public void insertDummyValues(){
        //insert some dummy values into out database for testing purposes.

        Toast.makeText(myContext, "inserting dummy values", Toast.LENGTH_LONG).show();
        SQLiteDatabase db = this.getWritableDatabase();

        //db.delete("marcnotes",null,null);

        ContentValues contentValues = new ContentValues();

        contentValues.put("notebook", "Linux");
        contentValues.put("tag", "None");
        contentValues.put("content", "Who should try Tumbleweed?\n" +
                "\n" +
                "Any user who wishes to have newer packages than are available in the openSUSE 12.3 repositories. This includes, but is not limited to, an updated Linux kernel, SAMBA, git, desktops, office applications and many other packages.\n" +
                "\n" +
                "Due to the Linux kernel being updated very frequently, users who rely on proprietary graphic drivers should not use the Tumbleweed repository unless they are familiar with updating these drivers from source on their own. See articles NVIDIA and ATI, section \"The hard way\", for how to do this if you are interested. Note that the additional Packman repositories are available for Tumbleweed! ");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 0);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues);
        contentValues.clear();

        contentValues.put("notebook", "Linux");
        contentValues.put("tag", "banana");
        contentValues.put("content", "Qemu\n" +
                "\n" +
                "(To proceed you should have a basic understanding of Windows console. Otherwise you should try the guide for VirtualBox)\n" +
                "If you don't have Qemu, then download QEMU on Windows from qemu.org\n" +
                "Extract the downloaded archive\n" +
                "Since we won't cover scripting here, for the ease of use move your ReactOS Installation CD (ReactOS.iso) to the Qemu directory\n" +
                "Run the console (cmd) and switch to Qemu directory (cd)\n" +
                "Create a new disk image using the qemu-img utility:\n" +
                "qemu-img create -f vmdk reactos.vmdk 1GB\n" +
                "Run Qemu with attached disk and loaded ISO:\n" +
                "qemu -L . -m 128 -hda reactos.vmdk -cdrom ReactOS.iso -boot d\n" +
                "ReactOS Setup program will start. Follow it's instructions.\n" +
                "Make sure you choose Install bootloader on the harddisk (MBR)\n" +
                "When setup program prompts for ENTER to reboot, exit Qemu\n" +
                "Now since ReactOS is installed, from now on you can run it using:\n" +
                "qemu -L . -m 128 -hda reactos.vmdk\n" +
                "Run ReactOS in default mode (ReactOS) unless you need to debug");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 0);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues);
        contentValues.clear();

        contentValues.put("notebook", "General");
        contentValues.put("tag", "None");
        contentValues.put("content", "blah blah blah");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 0);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues);
        contentValues.clear();

        contentValues.put("notebook", "General");
        contentValues.put("tag", "None");
        contentValues.put("content", "yada yada....");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 1);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues);
        contentValues.clear();

        contentValues.put("notebook", "Programming");
        contentValues.put("tag", "None");
        contentValues.put("content", "programming is fun....");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 0);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues);
        contentValues.clear();


        contentValues.put("notebook", "Life");
        contentValues.put("tag", "None");
        contentValues.put("content", "Life is life naa naa na na na....");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 0);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues);
        contentValues.clear();

        /*
        contentValues.put("notebook", "Addresses");
        contentValues.put("tag", "None");
        contentValues.put("content", "who would live in a house lie this....");
        contentValues.put("created", "2014-09-05 19:56");
        contentValues.put("modified", "2014-09-05 19:56");
        contentValues.put("pinned", 0);
        contentValues.put("BGColour", "#FFFFFF");

        db.insert("marcnotes", null, contentValues); */
        db.close();

        Toast.makeText(myContext, "finished inserting dummy values", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS books");

        // create fresh books table
        this.onCreate(db);
    }

    //----------------------------------------
    // return all the notebook names
    //---------------------------------------
    public String[] getNotebooks(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT DISTINCT notebook from marcnotes",null);
        String[] result = new String[c.getCount()];

        int idx=0;
        if (c.moveToFirst()) {
            do {
                result[idx++]=c.getString(c.getColumnIndex("notebook"));
            } while (c.moveToNext());
            db.close();
            return result;
        }
        db.close();
        return null;

    }

    //-----------------------------------------------
    // Get all the notes in a given notebook
    //-----------------------------------------------
    public List<ContentValues>  getNotesInNotebook(String notebook){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM marcnotes WHERE notebook = ? ORDER BY modified DESC", new String[] {notebook});
        List<ContentValues> rows= new ArrayList<ContentValues>();
        int idx=0;
        if (c.moveToFirst()) {
            do {
                ContentValues row= new ContentValues();
                row.put("noteID",c.getString(c.getColumnIndex("id")));
                row.put("notebook",c.getString(c.getColumnIndex("notebook")));
                row.put("tag",c.getString(c.getColumnIndex("tag")));
                row.put("content",c.getString(c.getColumnIndex("content")));
                row.put("created",c.getString(c.getColumnIndex("created")));
                row.put("modified",c.getString(c.getColumnIndex("modified")));
                row.put("pinned",c.getString(c.getColumnIndex("pinned")));
                row.put("BGColour",c.getString(c.getColumnIndex("BGColour")));
                rows.add(row);
            } while (c.moveToNext());
            db.close();
            return rows;
        }
        db.close();
        return null;

    }



    //-----------------------------------------------
    // Get all the note id's in a given notebook
    //-----------------------------------------------
    public List<String>  getNoteIds(String notebook){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT id FROM marcnotes WHERE notebook = ? ORDER BY modified DESC", new String[] {notebook});
        List<String> rows= new ArrayList<String>();
        int idx=0;
        if (c.moveToFirst()) {
            do {
                rows.add(c.getString(c.getColumnIndex("id")));
            } while (c.moveToNext());
            db.close();
            return rows;
        }
        db.close();
        return null;

    }


    //-----------------------------------------------
    // Get pinned notes
    //-----------------------------------------------
    public List<ContentValues>  getPinnedNotes(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM marcnotes WHERE pinned is 1 ORDER BY modified DESC",null);
        List<ContentValues> rows= new ArrayList<ContentValues>();
        int idx=0;
        if (c.moveToFirst()) {
            do {
                ContentValues row= new ContentValues();
                row.put("noteID",c.getString(c.getColumnIndex("id")));
                row.put("notebook",c.getString(c.getColumnIndex("notebook")));
                row.put("tag",c.getString(c.getColumnIndex("tag")));
                row.put("content",c.getString(c.getColumnIndex("content")));
                row.put("created",c.getString(c.getColumnIndex("created")));
                row.put("modified",c.getString(c.getColumnIndex("modified")));
                row.put("pinned",c.getString(c.getColumnIndex("pinned")));
                row.put("BGColour",c.getString(c.getColumnIndex("BGColour")));
                rows.add(row);
            } while (c.moveToNext());
            db.close();
            return rows;
        }
        db.close();
        return null;

    }


    //----------------------------------------------------
    // Get note from SQL ID.
    //----------------------------------------------------
    public ContentValues getNote(String sqlID){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM marcnotes WHERE id is ?",new String[]{sqlID});
        if (c.moveToFirst()) {
            ContentValues row= new ContentValues();
            row.put("noteID",c.getString(c.getColumnIndex("id")));
            row.put("notebook",c.getString(c.getColumnIndex("notebook")));
            row.put("tag",c.getString(c.getColumnIndex("tag")));
            row.put("content",c.getString(c.getColumnIndex("content")));
            row.put("created",c.getString(c.getColumnIndex("created")));
            row.put("modified",c.getString(c.getColumnIndex("modified")));
            row.put("pinned",c.getString(c.getColumnIndex("pinned")));
            row.put("BGColour",c.getString(c.getColumnIndex("BGColour")));
            db.close();
            return row;
        }

        db.close();
        return null;
    }

}
