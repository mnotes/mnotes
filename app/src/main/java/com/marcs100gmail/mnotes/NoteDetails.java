package com.marcs100gmail.mnotes;

/**
 * Created by marc on 06/10/14.
 */
public class NoteDetails {
    public String sqlID;
    public String notebook;
    public String tag;
    public String contents;
    public String created;
    public String modified;
    public String colour;
    public String pinned;



    public NoteDetails(String noteID, String notebook, String tag, String contents,
                       String created, String modified, String colour, String pinned) {
        this.sqlID = noteID;
        this.notebook = notebook;
        this.tag=tag;
        this.contents=contents;
        this.created=created;
        this.modified=modified;
        this.colour=colour;
        this.pinned=pinned;
    }
}